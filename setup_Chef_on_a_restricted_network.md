# Setup Chef on a restricted network

###### Table of Contents
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=5 orderedList=false} -->
<!-- code_chunk_output -->

* [Prep workstation for chef](#prep-workstation-for-chef)
* [Chef workstation](#chef-workstation)
* [Cleanup](#cleanup)

<!-- /code_chunk_output -->

## Prep workstation for chef
* Edit your `.bash_profile`
  ```
  echo 'TERM=xterm-256color' >> ~/.bash_profile
  echo 'eval "$(chef shell-init bash)"' >> ~/.bash_profile
  ```

* clone the `<chef_server>.pem` file
  ```
  git clone git@<doamin_&_namespace_of_project_where_chef_server_pem_is_stored>.git
  ```

* source the `.bash_profile` workstation
  ```
  source ~/.bash_profile
  ```

* __Note:__ There are times that a simple `source` will not work to make the changes to your `.bash_profile` live so you may need to logout and then log back in the workstation.

## Chef workstation
1. Get the Starter Kit from the chef server
  * __Note:__ if you can not sign in you may need to speak be added to the chef server (and the correct organization)
    * Sign into `https://<chef_server>/login`
      * Click on the `Administration` tab
      * Click on the correct organization, then click on the cog icon with the drop down arrow to the right
      * Select `Starter Kit`
      * Click on `Download Starter Kit`
      * You will be told that `Your user key will be reset. Are you sure you want to do this?` click on `Proceed`.
      * This will download a file called `chef-starter.zip` to your downloads folder.

2. `scp` the chef-starter.zip to your home dir on the corresponding workstation:
  * __Note:__ replace `<user>` with your username

    ```
    scp /cygdrive/c/Users/$USER/Downloads/chef-starter.zip <user>@<remote_server>:~/
    ```
    * The `chef-starter.zip` is now in your home dir on your chef workstation
    * Make sure to delete rename, or move `chef-starter.zip` from your downloads

3. `ssh` into the chef workstation
  * __Note:__ replace `<user>` with your username

    ```
    ssh <user>@<remote_server>
    ```
    * unzip the `chef-starter.zip` that you just pushed on to the workstation.

    ```
    unzip ~/chef-starter.zip
    ```
    * make a dir named `/chef-repo-master/` inside of `~/chef-repo/`

    ```
    mkdir -p ~/chef-repo/chef-repo-master/
    rm -rf ~/chef-starter.zip
    ```

4. Edit the .chef files
  * cd into `.chef` dir

    ```
    cd ~/chef-repo/.chef
    ```
  * Add missing info into knife.rb

    ```
    echo 'validation_client_name   "video_operations-validator"' >> ~/chef-repo/.chef/knife.rb
    echo 'validation_key           "#{current_dir}/video_operations-validator.pem"' >> ~/chef-repo/.chef/knife.rb
    ```
  * Place v`ideo_operations-validator.pem` inside your `.chef` dir

    ```
    mv ~/chef_video_operations-validator/<chef_server>/video_operations-validator.pem ~/chef-repo/.chef/video_operations-validator.pem
    ```

5. Get ssl certs and fix gem

    ```
    mkdir -p ~/chef-repo/.chef/trusted_certs
    knife ssl fetch
    knife ssl check
    gem source --remove https://rubygems.org/
    gem source --add http://<artifactory_server>/artifactory/api/gems/oe_gem/
    ```

6. Success!
  * You can validate that the initial setup with the following command while inside of the `~/chef-repo/chef-repo-master/` dir:

    ```
    knife node list
    ```

## Cleanup
  * Remove any extra dirs and files on <remote_server> that are not needed anymore

    ```
    rm -rf ~/chef_video_operations-validator/
    ```
