# Setup gitlab

#### This shows how to setup a ssh key, and authorize it for your user in gitlab.

1. `ssh` into the workstation and do prep.
  * **Note:** replace `<user>` with your username

    ```
    ssh <user>@<remote_workstation>
    cd ~/.ssh
    ssh-keygen -t rsa -f /home/<user>/.ssh/id_rsa
    ```
2. Ensure the perms on your keys are `500`

    ```
    chmod 500 ~/.ssh
    ll -a ~/.ssh
    ```
3. Copy contents of id_rsa.pub to the clipboard

    ```
    cat id_rsa.pub
    ```
4. Log into gitlab at: `<git_server>`
    * click the person/profile icon in top right hand corner
    * click ssh keys
    * add your public ssh key you generated on workstation

#### You should now be able to clone repos down from gitlab.
