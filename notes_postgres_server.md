# Postgres Server Tips

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

* [Log into the Postgres Server](#log-into-the-postgres-server)
	* [Web gui (phpPgAdmin)](#web-gui-phppgadmin)
	* [Bash](#bash)
* [Legend](#legend)
* [Queries](#queries)

<!-- /code_chunk_output -->

## Log into the Postgres Server

### Web gui (phpPgAdmin)

URL: http://<postgres_server>/phpPgAdmin/
Server: <postgres_server>:5432:allow
User: <user>
Database: <database>
Schema search path: public

### Bash
```
ssh <user>@<postgres_server>
psql -d <database> -U <user>
# Password for user <user>:
```

## Legend
| Variable | Explanation |
|---|---|
| `svr` | 3-4 letter server type prefix code. For a full list, visit this link: http____ |
| `kma4` | 4 letter KMA (Known Market Affected) code. For a full list, visit this link: http____ |
| `rdcwst` | 6 letter RDC (Regional Data Center) code which includes the State abbreviation. For a full list, visit this link: http____ |
| `rdc4.st` | 4 letter RDC (Regional Data Center) code and State abbreviation the RDC is in, separated by a `.` (e.g. pldc.or) |
| `lineup_id` | ID for a channel lineup |

## Queries

1. Find IP, FQDN and Location_CLLI for certain servers in a certain KMA
    ```
    SELECT "ip_addr","fqdn","location_clli" FROM "public"."device_view" WHERE "fqdn" ILIKE '<svr>%<kma4>%';
    ```
    * Example for #1 - Find all AMS servers in Kennewick Washington
      ```
      SELECT "ip_addr","fqdn","location_clli" FROM "public"."device_view" WHERE "fqdn" ILIKE 'ams%kenn%';
      ```

1. Find  IP, FQDN and Location_CLLI of all servers of a certain type in a certain RDC
    ```
    SELECT "ip_addr","fqdn","location_clli" FROM "public"."device_view" WHERE "fqdn" ILIKE '<svr>%' AND "location_clli" ILIKE '<rdcwst>';
    ```
    * Example for #2 - Find all cache servers in Portland Oregon
      ```
      SELECT "ip_addr","fqdn","location_clli" FROM "public"."device_view" WHERE "fqdn" ILIKE 'pxy%' AND "location_clli" ILIKE 'pldcor';
      ```
1. Find all servers of multiple types in a certain KMA
    ```
    SELECT "ip_addr","fqdn" FROM "public"."devices"
    WHERE "fqdn" LIKE 'ams%<kma>%<rdc.st>%'
    OR "fqdn" LIKE 'csm%<kma>%<rdc.st>%';
    ```
    * Example for #3 - Find all AMS and TSBroadcaster servers in Kennewick Washington
      * **Note:** This can be expanded to include more server types or more locations.
      ```
      SELECT "ip_addr","fqdn" FROM "public"."devices"
      WHERE "fqdn" LIKE 'ams%kenn%pldc.or%'
      OR "fqdn" LIKE 'dmc%kenn%pldc.or%';
      ```

1. Find all channels in the database for a specific lineup_id
    ```
    SELECT "lineup_id","ch_num","rovi_id","is_sdv","sd_hd_pair","cas_id" FROM "sgui"."lineup_svcs" WHERE "lineup_id" ILIKE '<lineup_id>';
    ```
    * Example for #4 - Find all channels in SLDL-7
      ```
      SELECT "lineup_id","ch_num","rovi_id","is_sdv","sd_hd_pair","cas_id" FROM "sgui"."lineup_svcs" WHERE "lineup_id" ILIKE 'SLDL-7';
      ```
