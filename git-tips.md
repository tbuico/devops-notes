# Git Tips
Tips for those already familiar with git but looking for better functionality

## Table of Contents
<!-- @import "[TOC]" {cmd="toc" depthFrom=3 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

* [Better Git Add](#better-git-add)
* [Commit Message with Date Programmatically](#commit-message-with-date-programmatically)
* [Better Git Push](#better-git-push)
* [Checkout Most Recent Branch](#checkout-most-recent-branch)

<!-- /code_chunk_output -->


### Better Git Add
This will add all files and subdirs with verbosity, which is useful for logging and understanding progress in large repos
```
git add -vA .
```

### Commit Message with Date Programmatically
Very useful of commiting in a script
```
git commit -m "$(date '+%F_%H-%M')"
```

### Better Git Push
This will push with verbosity and show progress, which is useful for logging and understanding progress in large repos
```
git push -v --progress
```

### Checkout Most Recent Branch
While in the git repo, pull all remote branches from origin and then find the branch that was most recently committed to. Most useful for scripting.
```
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done >/dev/null 2>&1
git fetch --all >/dev/null 2>&1
git pull --all >/dev/null 2>&1
recent_branch=($(git for-each-ref --count=30 --sort=-committerdate refs/heads/ --format='%(refname:short)'))
git checkout ${recent_branch[0]}
```
