# Best Practices for Optional use of Ansible

You can use Ansible on `<remote_server>` to send commands to all desired nodes. (_replacement for sudo_spam_cmd_)

1. ssh into `<remote_server>`.

    ```
    ssh <IPA_user>@<remote_server>
    ```

1. The inventory file can be viewed here:

    ```
    vim /etc/ansible/hosts
    ```

1. Make a dir for logs in your home dir if you do not already have one.

    ```
    mkdir -p ~/logs/ansible
    ```

1. Here is an example to run chef-client -w with stdout and print to a log file. Make note to replace variables as needed

    ```
    ansible '<unique_prefix>-<server_type>' -m shell -a "chef-client -w -F doc" -u <user> -b --become-user=root > ~/logs/ansible/<jira_ticket>_<unique_prefix>-<server_type>_why-run_$(date '+%F_%H-%M').log
    ```
  * **Note #1**: One can replace `"chef-client -w --force-formatter --force-logger"` with any command that needs to be ran on the list of servers. The ` --force-formatter --force-logger` portion is only needed when running chef commands to display *both* stdout/stderr *and* log to the targeted machine's `/var/log/chef-client.log` properly for logging purposes.

## ansible_wrapper method (more advanced)
Create the following as `ansible_wrapper.yaml` to wrap your desired remote command using ansible-playbook, enabling resuming and retries.

```
- hosts: "{{ lookup('env', 'ANSIBLE_HOSTLIST') }}"
  gather_facts: no
  become_method: sudo
  become: yes
  tasks:
    - shell: "{{ lookup('env', 'ANSIBLE_REMOTE_COMMAND') }}"
      register: outout_result
      until: outout_result.rc == 0
      retries: 3
      delay: 15
```

This enables execution as such:

```
ANSIBLE_REMOTE_COMMAND="chef-client -o recipe[cookbook-nrpe] -w --force-formatter --force-logger" \
ANSIBLE_HOSTLIST="gsaa*" \
ANSIBLE_STDOUT_CALLBACK=minimal \
ansible-playbook -f 25 ~/ansible_wrapper.yaml --list-hosts \
2>&1 | tee -a ~/logs/ansible/sguide3070_gsaa_why-run_$(date '+%F_%H-%M').log
```

  * **Note #1**: Use the backslash escape character as required in your `ANSIBLE_REMOTE_COMMAND`. Enclosing characters in double quotes preserves the literal value of all characters within the quotes, with the exception of $, `, \, ", and, when history expansion is enabled, !.`
  * **Note #2**: `ANSIBLE_HOSTLIST` supports wildcards and comma separated lists.
  * **Note #3**: `--list-hosts` will show which hosts FQDNs the command(s) will be executed against Remove this switch then run again after confirming the list looks correct to perform the actual execution.
  * **Note #4**: If your run fails on host(s), you will see the stdout/stderr details of the failure on the screen, along with the line `to retry, use: --limit @~/ansible_wrapper.retry`. After resolving the cause of the failure, append the displayed  `--limit @~/ansible_wrapper.retry` statement into your command, in place of the `--list-hosts`.
  * **Note #5**: When copying the multi-line command from above, ensure your text editor isn't placing your command all on one line or a space character after the slash, or bash's line wrapping will fail. You should have 5 lines as displayed above.
